import SwiftUI

struct CoverImage: Codable, Identifiable {
    // MARK: - ©Global-PROPERTIES
    //#..............................
    let id: Int
    let name: String
    //#..............................
}
