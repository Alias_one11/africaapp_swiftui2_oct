import SwiftUI

struct Animal: Codable, Identifiable {
    // MARK: - ∆Global-PROPERTIES
    //∆..............................
    var id = UUID().uuidString
    let name, headline, description,
        link, image: String
    let gallery, fact: [String]
    //∆..............................
}
