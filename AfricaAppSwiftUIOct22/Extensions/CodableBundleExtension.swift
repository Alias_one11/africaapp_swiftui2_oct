import SwiftUI

/**..............................................
 A representation of the code and resources
 stored in a bundle directory on disk.
 ..............................................*/

extension Bundle {
    ///# ........... Functions ...........
    // ∆ decode<T: Codable>: MUST CONFORM TO THE CODABLE PROTOCOL
    func decode<T: Codable>(_ file: String) -> T {
        //..........
        /// #1. Locate the json file
        guard let url = url(forResource: file, withExtension: nil) else {
            fatalError("*. Failed to locate file in bundle...")
        }
        
        /// #2. Create a property for the data
        guard let data = try? Data(contentsOf: url) else {
            fatalError("*. Failed to locate file in bundle...")
        }
        /// #3. Create a decoder
        let decoder = JSONDecoder()
        
        /// #4. Create a property for the decoded data
        guard let loadedCoverImages = try? decoder.decode(T.self, from: data) else {
            fatalError("*. Failed to locate file in bundle...")
        }

        /// #5. Return the decoded data
        // ∆ SINCE IT IS A GENERIC RETURN TYPE, WE CAN RETURN ANYTHING
        // ∆ ARRAYS,STRINGS...etc as long as the object conforms to the protocol
        return loadedCoverImages
    }
}
