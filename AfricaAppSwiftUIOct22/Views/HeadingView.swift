import SwiftUI

// MARK: - Preview
struct HeadingView_Previews: PreviewProvider {
    
    static var previews: some View {
        
        HeadingView(headingSFImg: "photo.on.rectangle.angled",
                    headingText: "Widerness in Pictures")
            //.padding(.all, 100)
        //.preferredColorScheme(.dark)
        .previewLayout(.sizeThatFits)
        //.previewLayout(.fixed(width: 320, height: 640))
        // The preview below is for like a card
        //.previewLayout(.fixed(width: 440, height: 270))
    }
}

struct HeadingView: View {
    // MARK: - ∆Global-PROPERTIES
    //∆..............................
    var headingSFImg, headingText: String
    //∆..............................
    
    var body: some View {
        //.............................
        HStack {
            //∆..........
            Image(systemName: headingSFImg)
                .foregroundColor(.accentColor)
                .imageScale(.large)
            
            Text(headingText)
                .font(.title3)
                .fontWeight(.bold)
            
        }///||END__PARENT-VSTACK||
        .padding(.vertical)
        //.............................
        
    }///-|_End Of body_|
    /*©-----------------------------------------©*/
    
}// END: [STRUCT]

/*©-----------------------------------------©*/

