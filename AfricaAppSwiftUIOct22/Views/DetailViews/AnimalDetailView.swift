import SwiftUI

// MARK: - Preview
struct AnimalDetailView_Previews: PreviewProvider {
    //∆..............................
    static let animals: [Animal] =
        Bundle.main.decode("animals.json")
    //∆..............................
    
    static var previews: some View {
        
        NavigationView {
            AnimalDetailView(animal: animals[0])
        }//.padding(.all, 100)
        //.preferredColorScheme(.dark)
        //.previewLayout(.sizeThatFits)
        //.previewLayout(.fixed(width: 320, height: 640))
        // The preview below is for like a card
        //.previewLayout(.fixed(width: 440, height: 270))
    }
}

struct AnimalDetailView: View {
    // MARK: - ∆Global-PROPERTIES
    //∆..............................
    let animal: Animal
    //∆..............................
    
    var body: some View {
        
        //.............................
        ScrollView(.vertical, showsIndicators: false) {
            //∆..........
            VStack(alignment: .center, spacing: 20) {
                //∆..........
                // MARK: -∆ HERO IMAGE ------------------------------------
                Image(animal.image)
                    .resizable()
                    .scaledToFit()
                    //∆..................................
                // MARK: -∆ TITLE ------------------------------------
                Text(animal.name.uppercased())
                    .font(.largeTitle)
                    .fontWeight(.heavy)
                    .multilineTextAlignment(.center)
                    .padding(.vertical, 8)
                    .foregroundColor(.primary)
                    .background(
                        Color.accentColor
                            .frame(height: 6)
                            .offset(y: 24)
                    )
                    //∆..................................
                // MARK: -∆ HEADLINE ------------------------------------
                Text(animal.headline)
                    .font(.headline)
                    .multilineTextAlignment(.leading)
                    .foregroundColor(.accentColor)
                    .padding(.horizontal)
                    //∆..................................
                // MARK: -∆ GALLERY ------------------------------------
                /// ∆ Extends the number of views from 0 to 9
                /// ∆ by Grouping anything larger then that amount
                Group {
                    //∆..........
                    // MARK: -∆ HeadingView
                    HeadingView(headingSFImg: "photo.on.rectangle.angled",
                                headingText: "Wilderness in Pictures")
                    
                    // MARK: -∆ InsetGalleryView
                    InsetGalleryView(animal: animal)
                }
                .padding(.horizontal)
                //∆..................................
                // MARK: -∆ FACTS ------------------------------------
                /// ∆ Extends the number of views from 0 to 9
                /// ∆ by Grouping anything larger then that amount
                Group {
                    //∆..........
                    // MARK: -∆ HeadingView
                    HeadingView(headingSFImg: "questionmark.circle",
                                headingText: "Did you know?")
                    
                    // MARK: -∆ InsetFactView
                    InsetFactView(animal: animal)
                }
                .padding(.horizontal)
                //∆..................................
                // MARK: -∆ DESCRIPTION ------------------------------------
                /// ∆ Extends the number of views from 0 to 9
                /// ∆ by Grouping anything larger then that amount
                Group {
                    //∆..........
                    // MARK: -∆ HeadingView
                    HeadingView(headingSFImg: "info.circle",
                                headingText: "All about \(animal.name)")
                    
                    // MARK: -∆ Description Text
                    Text(animal.description)
                        .bold()
                        .multilineTextAlignment(.leading)
                        .layoutPriority(1)
                    
                    
                }
                .padding(.horizontal)
                //∆..................................
                // MARK: -∆ MAP ------------------------------------
                Group {
                    //∆..........
                    // MARK: -∆ HeadingView
                    HeadingView(headingSFImg: "map",
                                headingText: "National Parks")
                    
                    // MARK: -∆ InsetMapView
                    InsetMapView()
                }
                
                // MARK: -∆ LINK ------------------------------------
                Group {
                    //∆..........
                    HeadingView(headingSFImg: "books.vertical",
                                headingText: "Learn More")
                    
                    ExternalWebLinkView(animal: animal)
                }
                .padding(.horizontal)
                
            }/// ∆ END VSTACK
            .navigationTitle("Learn about \(animal.name)")
            // ∆ Works with IOS ONLY
            .navigationBarTitleDisplayMode(.inline)
        }///||END__PARENT-SCROLLVIEW||
        
        //.............................
        
    }///-|_End Of body_|
    /*©-----------------------------------------©*/
    
}// END: [STRUCT]

/*©-----------------------------------------©*/

