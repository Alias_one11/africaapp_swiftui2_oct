import SwiftUI

// MARK: - Preview
struct ExternalWebLinkView_Previews: PreviewProvider {
    //∆..............................
    static let animals: [Animal] =
        Bundle.main.decode("animals.json")
    //∆..............................
    
    static var previews: some View {
        
        ExternalWebLinkView(animal: animals[0]).padding()
        //.preferredColorScheme(.dark)
        .previewLayout(.sizeThatFits)
        //.previewLayout(.fixed(width: 320, height: 640))
        // The preview below is for like a card
        //.previewLayout(.fixed(width: 440, height: 270))
    }
}

struct ExternalWebLinkView: View {
    // MARK: - ∆Global-PROPERTIES
    //∆..............................
    let animal: Animal
    //∆..............................
    
    var body: some View {
        
        //.............................
        GroupBox {
            //∆..........
            HStack {
                //∆..........
                // MARK: -∆ Left side of the HStack ------------
                Image(systemName: "globe")
                    .font(.system(size: 18, weight: .bold))

                
                Text("Wikipedia")
                    .bold()
                Spacer()// Spaced horizontally leading
                
                // MARK: -∆ Right side of the HStack ------------
                Group {
                    //∆..........
                    Image(systemName: "arrow.up.right.square")
                        .font(.system(size: 18, weight: .medium))
                    
                    Link(
                        animal.name,
                         destination: animal.link.asURL ??
                            "https://wikipedia.org".asURL!
                    )
                    .font(.system(size: 18, weight: .medium))

                }
                .foregroundColor(.accentColor)
                //∆..................................
            }/// ∆ END HSTACK
            //∆..........
        }///||END__PARENT-GROUPBOX||
        
        //.............................
        
    }///-|_End Of body_|
    /*©-----------------------------------------©*/
    
}// END: [STRUCT]

/*©-----------------------------------------©*/

