import SwiftUI

// MARK: - Preview
struct InsetFactView_Previews: PreviewProvider {
    //∆..............................
    static var animals: [Animal] = Bundle.main.decode("animals.json")
    //∆..............................
    
    static var previews: some View {
        
        InsetFactView(animal: animals[2]).padding()
        //.preferredColorScheme(.dark)
        .previewLayout(.sizeThatFits)
        //.previewLayout(.fixed(width: 320, height: 640))
        // The preview below is for like a card
        //.previewLayout(.fixed(width: 440, height: 270))
    }
}

struct InsetFactView: View {
    // MARK: - ∆Global-PROPERTIES
    //∆..............................
    let animal: Animal
    //∆..............................
    
    var body: some View {
        
        //.............................
        GroupBox {
            
            TabView {
                //∆..........
                ForEach(animal.fact, id: \.self) { item in
                    //∆..........
                    Text(item)
                        .bold()
                }
            }
            .tabViewStyle(PageTabViewStyle())
            .frame(idealWidth: 168, minHeight: 148, maxHeight: 180)
            
        }///||END__PARENT-GROUPBOX||
        
        //.............................
        
    }///-|_End Of body_|
    /*©-----------------------------------------©*/
    
}// END: [STRUCT]

/*©-----------------------------------------©*/

