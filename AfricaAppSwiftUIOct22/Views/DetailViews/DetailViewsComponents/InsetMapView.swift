import SwiftUI
import MapKit

// MARK: - Preview
struct InsetMapView_Previews: PreviewProvider {
    
    static var previews: some View {
        
        InsetMapView().padding()
            //.preferredColorScheme(.dark)
            .previewLayout(.sizeThatFits)
        //.previewLayout(.fixed(width: 320, height: 640))
        // The preview below is for like a card
        //.previewLayout(.fixed(width: 440, height: 270))
    }
}

struct InsetMapView: View {
    // MARK: - ∆Global-PROPERTIES
    //∆..............................
    @State private var region = MKCoordinateRegion(
        center: CLLocationCoordinate2D(
            latitude: 6.600286, longitude: 16.4377599),
        span: MKCoordinateSpan(
            latitudeDelta: 60,
            longitudeDelta: 60)
    )
    //∆..............................
    
    var body: some View {
        
        //.............................
        VStack(spacing: 8.0) {
            
            Map(coordinateRegion: $region)
            // MARK: - overlay
            //--|............................................
                .overlay(
                    NavigationLink(
                        destination: MapView(),
                        label: {
                            //∆..........
                            HStack {
                                // MARK: -∆ Map Pin
                                Image(systemName: "mappin.circle")
                                    .foregroundColor(.white)
                                    .imageScale(.large)
                                
                                // MARK: -∆ Location Text
                                Text("Location")
                                    .foregroundColor(.accentColor)
                                    .fontWeight(.bold)
                                
                            }/// ∆ END HSTACK
                            .padding(.vertical, 10)
                            .padding(.horizontal, 14)
                            .background(
                                Color.black
                                    .opacity(0.3)
                                    .cornerRadius(12)
                            )
                            //∆..................................
                        })/// ∆ END NAVIGATIONLINK
                        .padding(12)
                    , alignment: .topTrailing
                )
                //--|............................................
                .frame(height: 256)
                .cornerRadius(12)
            
        }///||END__PARENT-VSTACK||
        
        //.............................
        
    }///-|_End Of body_|
    /*©-----------------------------------------©*/
    
}// END: [STRUCT]

/*©-----------------------------------------©*/

