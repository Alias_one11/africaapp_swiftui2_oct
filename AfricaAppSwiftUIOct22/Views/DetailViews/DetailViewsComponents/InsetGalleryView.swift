import SwiftUI

// MARK: - Preview
struct InsetGalleryView_Previews: PreviewProvider {
    //∆..............................
    static var animal: [Animal] = Bundle.main.decode("animals.json")
    //∆..............................
    
    static var previews: some View {
        
        InsetGalleryView(animal: animal[0])//.padding(.all, 100)
        //.preferredColorScheme(.dark)
        .previewLayout(.sizeThatFits)
        //.previewLayout(.fixed(width: 320, height: 640))
        // The preview below is for like a card
        //.previewLayout(.fixed(width: 440, height: 270))
    }
}

struct InsetGalleryView: View {
    // MARK: - ∆Global-PROPERTIES
    //∆..............................
    let animal: Animal
    //∆..............................
    
    var body: some View {
        
        //.............................
        ScrollView(.horizontal, showsIndicators: false) {
            //∆..........
            HStack(alignment: .center, spacing: 15.0) {
                //∆..........
                ForEach(animal.gallery, id: \.self) { item in
                    Image(item)
                        .resizable()
                        .scaledToFit()
                        .frame(height: 200)
                        .cornerRadius(12)
                }/// ∆ END LOOP
            
            }/// ∆ END HSTACK
            
        }///||END__PARENT-ScrollView||
        
        //.............................
        
    }///-|_End Of body_|
    /*©-----------------------------------------©*/
    
}// END: [STRUCT]

/*©-----------------------------------------©*/
