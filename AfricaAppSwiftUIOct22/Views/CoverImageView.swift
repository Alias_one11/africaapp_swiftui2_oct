import SwiftUI

// MARK: - Preview
struct CoverImageView_Previews: PreviewProvider {
    
    static var previews: some View {
        
        CoverImageView()//.padding(.all, 100)
        //.preferredColorScheme(.dark)
        //.previewLayout(.sizeThatFits)
        .previewLayout(.fixed(width: 400, height: 300))
        // The preview below is for like a card
        //.previewLayout(.fixed(width: 440, height: 270))
    }
}

struct CoverImageView: View {
    // MARK: - ©Global-PROPERTIES
    //#..............................
    let coverImages: [CoverImage] = Bundle.main.decode("covers.json")
    //#..............................
    
    var body: some View {
        
        //............................./
        TabView {
            //..........
            ForEach(coverImages) { item in
                Image(item.name)
                    .resizable()
                    .scaledToFill()
            }/// # END LOOP
            
        }///||END__PARENT-TABVIEW||
        .tabViewStyle(PageTabViewStyle())
        //............................./
        
    }///-|_End Of body_|
    /*©-----------------------------------------©*/
    
}// END: [STRUCT]

/*©-----------------------------------------©*/
