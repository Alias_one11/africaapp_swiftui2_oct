import SwiftUI

// MARK: - Preview
struct ContentView_Previews: PreviewProvider {
            
    static var previews: some View {
        
        ContentView()//.padding(.all, 100)
        //.preferredColorScheme(.dark)
        //.previewLayout(.sizeThatFits)
        //.previewLayout(.fixed(width: 320, height: 640))
        // The preview below is for like a card
        //.previewLayout(.fixed(width: 440, height: 270))
    }
}

struct ContentView: View {
    // MARK: - ©Global-PROPERTIES
    //#..............................
    let animals: [Animal] = Bundle.main.decode("animals.json")
    //#..............................
    
    var body: some View {
        
        //............................./
        NavigationView {
            //..........
            List {
                //..........
                // MARK: -∆ CoverImageView
                CoverImageView()
                    .frame(height: 300)
                    .listRowInsets(
                        EdgeInsets(top: 0, leading: 0,
                                   bottom: 0, trailing: 0)
                    )
                /// ∆ All Animals shown vertically
                ForEach(animals) { animal in
                    //∆..........
                    NavigationLink(destination: AnimalDetailView(animal: animal)) {
                        //∆..........
                        AnimalListItemView(animal: animal)
                        
                    }/// ∆ END LINK
                }
                
            }/// # END LIST
            .navigationTitle("Africa")
            // ∆ Works with IOS ONLY
            .navigationBarTitleDisplayMode(.large)
            //#..................................
        }///||END__PARENT-NAVIGATIONVIEW||
        //............................./
        
    }///-|_End Of body_|
    /*©-----------------------------------------©*/
    
}// END: [STRUCT]

/*©-----------------------------------------©*/
// MARK: Helper Function
func iAmHere(myStr: String) -> some View {
    return Text("\(myStr)")
        .font(.system(size: 22))
        .foregroundColor(.black)
        .bold()
        .background(Color.white)
}
