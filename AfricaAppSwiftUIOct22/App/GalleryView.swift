import SwiftUI

// MARK: - Preview
struct GalleryView_Previews: PreviewProvider {
    
    static var previews: some View {
        
        GalleryView()//.padding(.all, 100)
        //.preferredColorScheme(.dark)
        //.previewLayout(.sizeThatFits)
        //.previewLayout(.fixed(width: 320, height: 640))
        // The preview below is for like a card
        //.previewLayout(.fixed(width: 440, height: 270))
    }
}

struct GalleryView: View {
    // MARK: - ©Global-PROPERTIES
    //#..............................
    
    //#..............................
    
    var body: some View {
        
        //............................./
        VStack(spacing: 8.0) {
            
            iAmHere(myStr: "GalleryView")
            
        }///||END__PARENT-VSTACK||
        
        //............................./
        
    }///-|_End Of body_|
    /*©-----------------------------------------©*/
    
}// END: [STRUCT]

/*©-----------------------------------------©*/
