import SwiftUI

// MARK: - Preview
struct VideoListView_Previews: PreviewProvider {
    
    static var previews: some View {
        
        VideoListView()//.padding(.all, 100)
        //.preferredColorScheme(.dark)
        //.previewLayout(.sizeThatFits)
        //.previewLayout(.fixed(width: 320, height: 640))
        // The preview below is for like a card
        //.previewLayout(.fixed(width: 440, height: 270))
    }
}

struct VideoListView: View {
    // MARK: - ©Global-PROPERTIES
    //#..............................
    
    //#..............................
    
    var body: some View {
        
        //............................./
        VStack(spacing: 8.0) {
            
            iAmHere(myStr: "VideoListView")
            
        }///||END__PARENT-VSTACK||
        
        //............................./
        
    }///-|_End Of body_|
    /*©-----------------------------------------©*/
    
}// END: [STRUCT]

/*©-----------------------------------------©*/
