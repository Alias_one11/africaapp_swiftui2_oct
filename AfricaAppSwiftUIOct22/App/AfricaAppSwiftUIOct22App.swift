//
//  AfricaAppSwiftUIOct22App.swift
//  AfricaAppSwiftUIOct22
//
//  Created by Jose Martinez on 10/22/20.
//

import SwiftUI

@main
struct AfricaAppSwiftUIOct22App: App {
    var body: some Scene {
        WindowGroup {
            MainView()
        }
    }
}
