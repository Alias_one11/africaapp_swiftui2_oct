import SwiftUI

// MARK: - Preview
struct MainView_Previews: PreviewProvider {
    
    static var previews: some View {
        
        MainView()//.padding(.all, 100)
        //.preferredColorScheme(.dark)
        //.previewLayout(.sizeThatFits)
        //.previewLayout(.fixed(width: 320, height: 640))
        // The preview below is for like a card
        //.previewLayout(.fixed(width: 440, height: 270))
    }
}

struct MainView: View {
    // MARK: - ©Global-PROPERTIES
    //#..............................
    
    //#..............................
    
    var body: some View {
        
        //............................./
        /// # A view that switches between multiple child
        ///  views using interactive user interface elements.
        TabView {
            //..........
            // MARK: -∂ ContentView
            ContentView()
                /// # Sets the tab bar item associated with this view.
                .tabItem {
                    Image(systemName: "square.grid.2x2")
                    Text("Browse")
                }
            
            // MARK: -∂ VideoListView
            VideoListView()
                .tabItem {
                    Image(systemName: "play.rectangle")
                    Text("Watch")
                }
            
            // MARK: -∂ MapView
            MapView()
                .tabItem {
                    Image(systemName: "map")
                    Text("Location")
                }
            
            // MARK: -∂ GalleryView
            GalleryView()
                .tabItem {
                    Image(systemName: "photo")
                    Text("Gallery")
                }
            
        }///||END__PARENT-TABVIEW||
        
        //............................./
        
    }///-|_End Of body_|
    /*©-----------------------------------------©*/
    
}// END: [STRUCT]

/*©-----------------------------------------©*/
